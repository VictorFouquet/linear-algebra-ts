# Linear Algebra Library

Simple library implementing linear algebra classes.

Currently included:
- Vector base class and interface
- Vector2 class and interface
- Vector3 class and interface

Coming soon:
- Matrix base class and interface

## Development

Install required packages

```
npm install
```

### Scripts

```
npm test
```

Runs unit tests for the library with Jest.

```
npm run compile
```

Compiles library to both ES5 and ES6 formats.

Output scripts will respectively be placed in `dist/lib/es5` and `dist/lib/es6`.

All classes can be imported directly from their respective `index.js` file.
