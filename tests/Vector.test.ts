import { Vector } from "../src/index"

describe('Testing Vector', () => {
    it('Should make a deep copy of a vector', () => {
        const v1 : Vector = new Vector([1, 2, 3, 4]);
        const v2 : Vector = v1.copy();

        // Checks that v1 is not v2
        expect(v1).not.toBe(v2);
        // Checks that v1 components are not v2 components
        expect(v1.components).not.toBe(v2.components);
        // Checks that v1 components have the same values as v2 components
        expect(v1.components).toEqual(v2.components);
    })

    it('Should perform vector additions', () => {
        const v1Components : number[] = [1, 2, 3, 4];
        const v2Components : number[] = [0, 3, 5, 6];
        const expected     : number[] = [1, 5, 8, 10];

        const v1 : Vector = new Vector(v1Components);
        const v2 : Vector = new Vector(v2Components);

        const v3 : Vector = v1.add(v2);

        // Checks that the add operation was correctly performed
        expect(v3.components).toEqual(expected);
        // Checks that none of v1 or v2 components have been changed
        expect(v1.components).toEqual(v1Components);
        expect(v2.components).toEqual(v2Components);
    })

    it('Should perform vector substractions', () => {
        const v1Components  : number[] = [ 1,  2,  9,  4];
        const v2Components  : number[] = [ 0,  3,  5,  6];
        const v1_v2Expected : number[] = [ 1, -1,  4, -2];
        const v2_v1Expected : number[] = [-1,  1, -4,  2];

        const v1 : Vector = new Vector(v1Components);
        const v2 : Vector = new Vector(v2Components);

        const v3 : Vector = v1.sub(v2);
        const v4 : Vector = v2.sub(v1);

        // Checks that the subtract operations were correctly performed
        expect(v3.components).toEqual(v1_v2Expected);
        expect(v4.components).toEqual(v2_v1Expected);
        // Checks that none of v1 or v2 components have been changed
        expect(v1.components).toEqual(v1Components);
        expect(v2.components).toEqual(v2Components);
    })

    it('Should perform vector scalings', () => {
        const vComponents  : number[] = [2, 4,  6,  8];
        const vMulExpected : number[] = [4, 8, 12, 16];
        const vDivExpected : number[] = [1, 2,  3,  4];

        const v1 : Vector = new Vector(vComponents);

        const v2 : Vector = v1.scale(2);
        const v3 : Vector = v1.scale(1/2);

        // Checks that the scaling operations were correctly performed
        expect(v2.components).toEqual(vMulExpected);
        expect(v3.components).toEqual(vDivExpected);
        // Checks that v1 components have not been changed
        expect(v1.components).toEqual(vComponents);
    })

    it('Should compute a vector\'s magnitude', () => {
        const v0 : Vector = new Vector([ 0,  0,  0,  0]);
        const v1 : Vector = new Vector([ 3,  9,  5, 10]);
        const v2 : Vector = new Vector([-1, -9, -7, -3]);
        const v3 : Vector = new Vector([-4, 13, -9,  0]);

        // Checks magnitude of null vector
        expect(v0.mag()).toBe(0);
        // Checks magnitude of vector with only positive components
        expect(v1.mag()).toBeCloseTo(14.662878299);
        // Checks magnitude of vector with only negative components
        expect(v2.mag()).toBeCloseTo(11.832159566);
        // Checks magnitude of vector with positive, negative and 0 components
        expect(v3.mag()).toBeCloseTo(16.30950643);
    })

    it('Should set a vector\'s magnitude', () => {
        // Checks case for null vector
        const v0 : Vector = new Vector([ 0, 0,  0, 0]);
        const v0Expected : number[] = [0, 0, 0, 0]; // Null vector shouldn't change
        v0.setMag(10);
        expect(v0.components).toEqual(v0Expected);
        expect(v0.mag()).toBe(0);
        
        // Checks conversion to null vector
        const v1 : Vector = new Vector([ 5, 3, -1, 9]);
        const v1Expected : number[] = [0, 0,-0, 0]; // Vector set to null vector with mag 0 
        v1.setMag(0);
        expect(v1.components).toEqual(v1Expected);
        expect(v1.mag()).toBe(0);
        
        // Checks error if a negative number is given as argument
        const v2 : Vector = new Vector([ 5, 3, -1, 9]);
        expect(() => v2.setMag(-1)).toThrow("Magnitude should always be greater than or equal to 0, received -1.");
        
        // Checks simple case with only one non-zero component in vector
        const v3 : Vector = new Vector([25, 0,  0, 0]);
        const v3Expected : number[] = [2, 0, 0, 0]; // Vector with one non-zero component and mag 2
        v3.setMag(2);
        expect(v3.components).toEqual(v3Expected);
        expect(v3.mag()).toBe(2);
        
        // Checks case with positive and negative components
        const v4 : Vector = new Vector([ 5, 3, -1, 9]);
        const v4Expected : number[] = [2.321, 1.3925, -0.464, 4.178]; // Vector with pos and neg components with mag 5
        v4.setMag(5);
        for (let i : number = 0; i < v4.components.length; i++) {
            expect(v4.components[i]).toBeCloseTo(v4Expected[i]);
        }
        expect(v4.mag()).toBe(5);
    })

    it('Should compute distance between two vectors', () => {
        const v0 : Vector = new Vector([ 0,  0,  0,  0]);
        const v1 : Vector = new Vector([ 5,  0,  0,  0]);
        const v2 : Vector = new Vector([ 3,  1, -9,  2]);
        const v3 : Vector = new Vector([-4,  2, -1,  4]);

        // Checks distance of vector with only one component to origin
        expect(v1.dist(v0)).toBe(5);
        expect(v0.dist(v1)).toBe(5);

        // Distance from v3 to v2 should be computed with
        // d = √((−4−3)^2 + (2−1)^2 + (−1+9)^2 + (4−2)^2)
        // d = √((−7)^2 + 1^2 + 8^2 + 2^2)
        // d = √(49 + 1 + 64 + 4)
        // d = √118
        // d = 10,862780491
        expect(v3.dist(v2)).toBeCloseTo(10.862780491);
        expect(v2.dist(v3)).toBeCloseTo(10.862780491);
    })

    it('Should compute a unit vector from a vector', () => {
        const v0 : Vector = new Vector([ 0, 0, 0, 0]);
        const v1 : Vector = new Vector([ 5, 0, 0, 0]);
        const v2 : Vector = new Vector([-5, 1, 4, 0, -9]);

        // Null vector should stay a null vector
        const v0Prime = v0.unit();
        expect(v0Prime.components).toEqual(v0.components);
        expect(v0Prime.components).not.toBe(v0.components);

        // Checks for a simple Vector with only one non-zero component 
        const v1Prime = v1.unit();
        expect(v1Prime.components).toEqual([1, 0, 0, 0]);
        expect(v1Prime.components).not.toEqual(v1.components);
        expect(v1Prime.mag()).toBe(1);

        // Checks for a vector with pos, neg, and zero components
        // m = √((−5)^2 + 1^2 + 4^2 + 0^2 + (-9)^2)
        // m = √(25 + 1 + 16 + 0 + 81)
        // m = √123
        // m = 11.0905
        // c = [-5 ÷ 11.0905, 1 ÷ 11.0905, 4 ÷ 11.0905, 0 ÷ 11.0905, -9 ÷ 11.0905]
        // c = [-0.4508, 0.0902, 0.3607, 0, -0.8115]
        const expected = [-0.4508, 0.0902, 0.3607, 0, -0.8115];
        const v2Prime = v2.unit();
        for (let i : number = 0; i < v2Prime.components.length; i++)
            expect(v2Prime.components[i]).toBeCloseTo(expected[i]);
        expect(v2Prime.components).not.toEqual(v2.components);
        expect(v2Prime.mag()).toBe(1);
    })

    it('Should convert a vector to unit vector', () => {
        const v0 : Vector = new Vector([ 0, 0, 0, 0]);
        const v1 : Vector = new Vector([ 5, 0, 0, 0]);
        const v2 : Vector = new Vector([-5, 1, 4, 0, -9]);

        // Null vector should stay a null vector
        const v0Prime = v0.toUnit();
        expect(v0Prime.components).toEqual(v0.components);
        expect(v0Prime.components).toBe(v0.components);
        expect(v0Prime).toBe(v0);

        // Checks for a simple Vector with only one non-zero component 
        const v1Prime = v1.toUnit();
        expect(v1Prime.components).toEqual([1, 0, 0, 0]);
        expect(v1Prime.components).toEqual(v1.components);
        expect(v1Prime).toBe(v1);

        // Checks for a vector with pos, neg, and zero components
        const expected = [-0.4508, 0.0902, 0.3607, 0, -0.8115];
        const v2Prime = v2.toUnit();
        for (let i : number = 0; i < v2Prime.components.length; i++)
            expect(v2Prime.components[i]).toBeCloseTo(expected[i]);
        expect(v2Prime.components).toEqual(v2.components);
        expect(v2Prime).toBe(v2);
    })

    it('Should compute the dot products between two vectors', () => {
        // Checks for error if two vectors have different dimensions
        const v0 : Vector = new Vector([0, 0]);
        const v1 : Vector = new Vector([0, 0, 0]);
        expect(() => v0.dot(v1)).toThrow(
            'Please provide vectors of same dimension. First has 2 components whereas second has 3.'
        );

        // Checks for dot products between two vectors of same dimensions
        const v2 : Vector = new Vector([ 4, -5,  0,  9, -3,  5]);
        const v3 : Vector = new Vector([-1,  4,  5, -5,  6,  3]);
        expect(v2.dot(v3)).toBe(-72);
        expect(v3.dot(v2)).toBe(-72);
    })

    it('Should compute angle between two vectors', () => {
        const v0 : Vector = new Vector([ 0,  0,  0,  0]);
        const v1 : Vector = new Vector([ 1,  5, -3,  2]);
        const v2 : Vector = new Vector([ 4, -3,  0,  2]);
        const v3 : Vector = new Vector([ 7,  9, -2,  3]);

        // Checks for error if one vector is null vector
        expect(() => v0.angleTo(v1)).toThrow('First vector has a magnitude of zero, leading to division by zero');
        expect(() => v1.angleTo(v0)).toThrow('Second vector has a magnitude of zero, leading to division by zero');

        // Mag of v1 is 6.2449
        // Mag of v2 is 5.3851
        // Mag of v3 is 11.9582

        // Dot product of v1.v2 = -7
        // Angle between v1 and v2 is arccos(v1 . v2 / (|v1| * |v2|))
        // angle = arccos( -7 / (6.2449 * 5.381))
        // angle = 1.7804
        expect(v1.angleTo(v2)).toBeCloseTo(1.7804);
        expect(v2.angleTo(v1)).toBeCloseTo(1.7804);

        // Dot product of v1.v3 = 64
        // Angle between v1 and v3 is arccos(v1 . v3 / (|v1| * |v3|))
        // angle = arccos(64 / (6.2449 * 11.9582))
        // angle = 0.5413
        expect(v1.angleTo(v3)).toBeCloseTo(0.5413);
        expect(v3.angleTo(v1)).toBeCloseTo(0.5413);
        
        // Dot product of v2.v3 = 7
        // Angle between v2 and v3 is arccos(v2 . v3 / (|v2| * |v3|))
        // angle = arccos( 7 / (5.381 * 11.9582))
        // angle = 1.4617
        expect(v2.angleTo(v3)).toBeCloseTo(1.4617);
        expect(v3.angleTo(v2)).toBeCloseTo(1.4617);
    })
})
