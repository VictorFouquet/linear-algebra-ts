import { Vector3 } from "../src/index";

describe('Testing Vector3', () => {
    it('Should hold x, y and z named components', () => {
        const v : Vector3 = new Vector3(5, 8, 4);

        expect(v.x).toBe(5);
        expect(v.y).toBe(8);
        expect(v.z).toBe(4);
    })

    it('Should copy a vector', () => {
        const v1 : Vector3 = new Vector3(3, 2, 9);
        const v2 : Vector3 = v1.copy();

        expect(v2.x).toBe(3);
        expect(v2.y).toBe(2);
        expect(v2.z).toBe(9);
        expect(v2).not.toBe(v1);
    })

    it('Should add two vectors', () => {
        const v1 : Vector3 = new Vector3( 3, 2, 5);
        const v2 : Vector3 = new Vector3(-1, 1, 4);
        const v3 : Vector3 = v1.add(v2);
        
        expect(v3.x).toBe(2);
        expect(v3.y).toBe(3);
        expect(v3.z).toBe(9);
    })

    it('Should subtract two vectors', () => {
        const v1 : Vector3 = new Vector3( 3,  2,  1);
        const v2 : Vector3 = new Vector3(-1,  1, -3);
        const v3 : Vector3 = v1.sub(v2);

        expect(v3.x).toBe(4);
        expect(v3.y).toBe(1);
        expect(v3.z).toBe(4);
    })

    it('Should scale a vector', () => {
        const v1 : Vector3 = new Vector3(3, 2, -1);
        const v2 : Vector3 = v1.scale(4);

        expect(v2.x).toBe(12);
        expect(v2.y).toBe(8);
        expect(v2.z).toBe(-4);
    })

    it('Should compute unit vector from a vector', () => {
        const v0 : Vector3 = new Vector3(6, -4, 8);
        const v1 : Vector3 = v0.unit();

        expect(v1.x).toBeCloseTo(0.5571);
        expect(v1.y).toBeCloseTo(-0.3714);
        expect(v1.z).toBeCloseTo(0.7428)
        expect(v1.mag()).toBeCloseTo(1);
        expect(v0.mag()).toBeCloseTo(10.7703);
    })

    it('Should convert vector to unit vector', () => {
        const v0 : Vector3 = new Vector3(6, -4, 8);
        v0.toUnit();
        expect(v0.x).toBeCloseTo(0.5571);
        expect(v0.y).toBeCloseTo(-0.3714);
        expect(v0.z).toBeCloseTo(0.7428)
        expect(v0.mag()).toBeCloseTo(1);
    })

    it('Should set a vector\'s magnitude', () => {
        const v0 : Vector3 = new Vector3(9, -1, 3);
        v0.setMag(3);
        expect(v0.x).toBeCloseTo(2.8304);
        expect(v0.y).toBeCloseTo(-0.3185);
        expect(v0.z).toBeCloseTo(0.9435);
        expect(v0.mag()).toBeCloseTo(3);
    })

    it('Should compute cross product between 2 vectors', () => {
        const v0 : Vector3 = new Vector3( 0,  0,  0);
        const v1 : Vector3 = new Vector3( 1,  5,  8);
        const v2 : Vector3 = new Vector3( 8, -4, -3);
        const v3 : Vector3 = new Vector3(-1, -1,  6);

        // Crossing with null vector should return null vector;
        const v0Prime = v0.cross(v1);
        expect(v0Prime.x).toBe(0);
        expect(v0Prime.y).toBe(0);
        expect(v0Prime.z).toBe(0);
        // v1 x v2 = [
        //    5 * -3 - 8 * -4 = -15 + 32 =  17, 
        //    8 *  8 - 1 * -3 =  64 +  3 =  67,
        //    1 * -4 - 5 *  8 =  -4 - 40 = -44
        // ]
        const v4 = v1.cross(v2);
        expect(v4.x).toBe(17);
        expect(v4.y).toBe(67);
        expect(v4.z).toBe(-44);
        // v2 x v1 = [
        //    -4 * 8 - -3 * 5 = -32 + 15 = -17,
        //    -3 * 1 -  8 * 8 =  -3 - 64 = -67,
        //     8 * 5 - -4 * 1 =  40 +  4 =  44
        // ]
        const v5 = v2.cross(v1);
        expect(v5.x).toBe(-17);
        expect(v5.y).toBe(-67);
        expect(v5.z).toBe(44);
        // v1 x v3 = [
        //    5 *  6 - 8 * -1 = 30 + 8 =  38,
        //    8 * -1 - 1 *  6 = -8 - 6 = -14,
        //    1 * -1 - 5 * -1 = -1 + 5 =   4
        // ]
        const v6 = v1.cross(v3);
        expect(v6.x).toBe(38);
        expect(v6.y).toBe(-14);
        expect(v6.z).toBe(4)
        // v3 x v1 = [
        //    -1 * 8 -  6 * 5 = -8 - 30 = -38,
        //     6 * 1 - -1 * 8 =  6 +  8 =  14,
        //    -1 * 5 - -1 * 1 = -5 +  1 =  -4
        // ]
        const v7 = v3.cross(v1);
        expect(v7.x).toBe(-38);
        expect(v7.y).toBe(14);
        expect(v7.z).toBe(-4);
        // v2 x v3 = [
        //    -4 *  6 - -3 * -1 = -24 -  3 = -27,
        //    -3 * -1 -  8 *  6 =   3 - 48 = -45,
        //     8 * -1 - -4 * -1 =  -8 -  4 = -12
        // ]
        const v8 = v2.cross(v3);
        expect(v8.x).toBe(-27);
        expect(v8.y).toBe(-45);
        expect(v8.z).toBe(-12);
        // v3 x v2 = [
        //    -1 * -3 -  6 * -4 =  3 + 24 = 27,
        //     6 *  8 - -1 * -3 = 48 -  3 = 45,
        //    -1 * -4 - -1 *  8 =  4 +  8 = 12
        // ]
        const v9 = v3.cross(v2);
        expect(v9.x).toBe(27);
        expect(v9.y).toBe(45);
        expect(v9.z).toBe(12);
    })
})
