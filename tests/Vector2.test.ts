import { Vector2 } from "../src/index"

describe('Testing Vector2', () => {
    it('Should hold x and y named components', () => {
        const v : Vector2 = new Vector2(5, 8);

        expect(v.x).toBe(5);
        expect(v.y).toBe(8);
    })

    it('Should copy a vector', () => {
        const v1 : Vector2 = new Vector2(3, 2);
        const v2 : Vector2 = v1.copy();

        expect(v2.x).toBe(3);
        expect(v2.y).toBe(2);
        expect(v2).not.toBe(v1);
    })

    it('Should add two vectors', () => {
        const v1 : Vector2 = new Vector2( 3, 2);
        const v2 : Vector2 = new Vector2(-1, 1);
        const v3 : Vector2 = v1.add(v2);

        expect(v3.x).toBe(2);
        expect(v3.y).toBe(3);
    })

    it('Should subtract two vectors', () => {
        const v1 : Vector2 = new Vector2( 3, 2);
        const v2 : Vector2 = new Vector2(-1, 1);
        const v3 : Vector2 = v1.sub(v2);

        expect(v3.x).toBe(4);
        expect(v3.y).toBe(1);
    })

    it('Should scale a vector', () => {
        const v1 : Vector2 = new Vector2( 3, 2);
        const v2 : Vector2 = v1.scale(4);

        expect(v2.x).toBe(12);
        expect(v2.y).toBe(8);
    })

    it('Should compute unit vector from a vector', () => {
        const v0 : Vector2 = new Vector2(3, 1);
        const v1 : Vector2 = v0.unit();

        expect(v1.x).toBeCloseTo(0.9486);
        expect(v1.y).toBeCloseTo(0.3162);
        expect(v1.mag()).toBeCloseTo(1);
        expect(v0.mag()).toBeCloseTo(3.1623);
    })

    it('Should convert vector to unit vector', () => {
        const v0 : Vector2 = new Vector2(3, 1);
        v0.toUnit();

        expect(v0.x).toBeCloseTo(0.9486);
        expect(v0.y).toBeCloseTo(0.3162);
        expect(v0.mag()).toBeCloseTo(1);
    })

    it('Should set a vector\'s magnitude', () => {
        const v0 : Vector2 = new Vector2(4, 6);

        expect(v0.mag()).not.toBeCloseTo(5);
        
        v0.setMag(5)

        expect(v0.mag()).toBeCloseTo(5);
        expect(v0.x).toBeCloseTo(2.7735);
        expect(v0.y).toBeCloseTo(4.1602);
    })

    it('Should compute cross product between 2 vectors', () => {
        const v0 : Vector2 = new Vector2( 0,  0);
        const v1 : Vector2 = new Vector2( 5,  8);
        const v2 : Vector2 = new Vector2(-4, -3);
        const v3 : Vector2 = new Vector2(-1,  6);

        // Crossing with null vector should be 0
        expect(v0.cross(v1)).toBe(0);

        // v1 x v2 = 5 * -3 - 8 * -4
        // v1 x v2 = -15 + 32 = 17
        expect(v1.cross(v2)).toBe(17);
        // v2 x v1 = -4 * 8 - -3 * 5
        // v2 x v1 = -32 + 15 = -17
        expect(v2.cross(v1)).toBe(-17);
        // v1 x v3 = 5 * 6 - 8 * -1
        // v1 x v3 = 30 + 8 = 38
        expect(v1.cross(v3)).toBe(38);
        // v3 x v1 = -1 x 8 - 6 * 5
        // v3 x v1 = -8 - 30 = -38
        expect(v3.cross(v1)).toBe(-38);
        // v2 x v3 = -4 * 6 - -3 * -1
        // v2 x v3 = -24 - 3 = -27
        expect(v2.cross(v3)).toBe(-27);
        // v3 x v2 = -1 * -3 - 6 * -4
        // v3 x v2 = 3 + 24 = 27
        expect(v3.cross(v2)).toBe(27);
    })

    it('Should compute signed angle between vector and positive x axis', () => {
        const v0 : Vector2 = new Vector2( 0,  0);
        const v1 : Vector2 = new Vector2( 1,  0);
        const v2 : Vector2 = new Vector2( 1,  1);
        const v3 : Vector2 = new Vector2( 0,  1);
        const v4 : Vector2 = new Vector2(-1,  1);
        const v5 : Vector2 = new Vector2(-1,  0);
        const v6 : Vector2 = new Vector2(-1, -1);
        const v7 : Vector2 = new Vector2( 0, -1);
        const v8 : Vector2 = new Vector2( 1, -1);
        
        expect(v0.signedAngle()).toBe(0)
        expect(v1.signedAngle()).toBe(0)              //   0 degrees
        expect(v2.signedAngle()).toBeCloseTo(0.7853)  //  +45 degrees
        expect(v3.signedAngle()).toBeCloseTo(1.5708)  //  +90 degrees
        expect(v4.signedAngle()).toBeCloseTo(2.3561)  // +135 degrees
        expect(v5.signedAngle()).toBeCloseTo(3.1415)  //  180 degrees
        expect(v6.signedAngle()).toBeCloseTo(-2.3561) // -135 degrees
        expect(v7.signedAngle()).toBeCloseTo(-1.5708) //  -90 degrees
        expect(v8.signedAngle()).toBeCloseTo(-0.7853) //  -45 degrees 
    })

    it('Should compute signed angles between vectors', () => {
        const v0 : Vector2 = new Vector2(0, 0);
        const v1 : Vector2 = new Vector2(1, 0);
        const v2 : Vector2 = new Vector2(1, 1);
        const v3 : Vector2 = new Vector2(0, 1);
        const v4 : Vector2 = new Vector2(-1, 1);
        const v5 : Vector2 = new Vector2(-1, 0);
        const v6 : Vector2 = new Vector2(1, -1);

        expect(v1.signedAngleTo(v1)).toBe(0)              //   0 degrees
        expect(v1.signedAngleTo(v2)).toBeCloseTo(0.7853)  //  +45 degrees
        expect(v2.signedAngleTo(v1)).toBeCloseTo(-0.7853) //  -45 degrees
        expect(v1.signedAngleTo(v3)).toBeCloseTo(1.5708)  //  +90 degrees
        expect(v3.signedAngleTo(v1)).toBeCloseTo(-1.5708) //  -90 degrees
        expect(v1.signedAngleTo(v4)).toBeCloseTo(2.3561)  // +135 degrees
        expect(v4.signedAngleTo(v1)).toBeCloseTo(-2.3561) // -135 degrees
        expect(v1.signedAngleTo(v5)).toBeCloseTo(3.1415)  //  180 degrees
        expect(v5.signedAngleTo(v1)).toBeCloseTo(3.1415)  //  180 degrees

        expect(v4.signedAngleTo(v2)).toBeCloseTo(-1.5708) //  -90 degrees
        expect(v6.signedAngleTo(v2)).toBeCloseTo(1.5708)  //  +90 degrees
    })

    it('Sould rotate a vector around origin', () => {
        const v1 : Vector2 = new Vector2(1, 0);
        const v2 : Vector2 = v1.rotate(1.5708);  // +90 degrees
        const v3 : Vector2 = v1.rotate(-1.5708); // -90 degrees

        expect(v2.x).toBeCloseTo(0);
        expect(v2.y).toBeCloseTo(1);
        expect(v3.x).toBeCloseTo(0);
        expect(v3.y).toBeCloseTo(-1);
    })

    it('Should rotate a vector around another vector', () => {
        const v1 : Vector2 = new Vector2(5, 4);
        const pivot : Vector2 = new Vector2(4, 4);
        const v2 : Vector2 = v1.rotateAround(1.5708, pivot);  // +90 degrees
        const v3 : Vector2 = v1.rotateAround(-1.5708, pivot); // -90 degrees

        expect(v2.x).toBeCloseTo(4);
        expect(v2.y).toBeCloseTo(5);
        expect(v3.x).toBeCloseTo(4);
        expect(v3.y).toBeCloseTo(3);
    })
})
